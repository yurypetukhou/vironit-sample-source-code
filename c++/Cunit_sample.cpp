/*
 
      Implementation of AI for a Unit which fights against a user.
      Simple simulation for more user engagement.
 
*/
void UnitBattleAI::Process() {
    if ((_unit != nullptr) && (_unit->InsideTransport == WORDNULL)) {
        
        // Check unit's queue for the next event
        _unit->ProcessActionQueueTikMoment();
        
        UNITID robjectid = _unit->get_robjectid();
        
        if (_unit->raction == atAttackUnit && robjectid == WORDNULL && _unit->action != atDelete) {
            _unit->Set_Instantly_Stop();
        }
        
        
        // can we get closer or can we attack immediately
        if ((_unit->action == atStop) && (_unit->raction == atStop)) {
            robjectid = _unit->get_robjectid();
            float minr = maxlongintc;
            US k = WORDNULL;
            int maxind = 0, ind;
            for (int j = 0; j < CMAXUNIT + CMAXLOCATION ; j++) {
                LOBJECT unit_j = Observer->status->object[j];
                if ((unit_j) && (unit_j->InsideTransport == WORDNULL) && (unit_j->fdestroy == false) && (IsEnemyTeams(Observer, unit_j->GetTeamID(), _unit->GetTeamID()))
                    && (_unit->SC_NeedGoCloser(j) == false) && (ind = _unit->IsCanAttack(j)) && ((ind > maxind) || ((ind == maxind) && ((rastpnt(_unit->pnt, unit_j->pnt)) < minr)))) {
                    maxind = ind;
                    minr = rastpnt(_unit->pnt, unit_j->pnt);
                    k = j;
                };
            }
            if (k != WORDNULL && maxind > 1)
                _unit->S_AttackUnit(k);
        }
        
        // Remove a unit from battlefield if command was sent.
        if (_unit->action == atDelete) {
            _unit->timetodelete -= ttik;
            if ((_unit->timetodelete <= 1.0f) && (_unit->timetodelete + ttik > 1.0f))
                _unit->Effect.Add((LEFFECT_OBJECT) new EFFECT_OBJECT_TELEPORT(Observer->status, _unit, _unit->GetTeamID()));
            if ((_unit->timetodelete <= 0.0f) && (_unit->timetodelete + ttik > 0.0f)) {
                _unit->fdel = true;
                _unit->Delete();
            }
        }
        
        // Move a unit closer to captrure a location or instantly stop a unit
        if (_unit->action == atMoveToCaptureBld) {
            robjectid = _unit->get_robjectid();
            if (robjectid != WORDNULL) {
                if (_unit->SC_NeedGoCloser(robjectid) == false) {
                    LLOCATION location = Observer->status->location[robjectid];
                    if (location && location->BeginCapture(_unit))
                        _unit->raction = atCaptureBld;
                    else {
                        _unit->Set_Instantly_Stop();
                    }
                }
            } else {
                _unit->Set_Instantly_Stop();
            }
        }
        
        
        // Attack logic
        if ((_unit->action == atAttackUnit) || (_unit->action == atAttackPnt) || (_unit->action == atPatrolToPnt)) {
            //majority time this branch is active
            robjectid = _unit->get_robjectid();
            if (_unit->deltaCirlingAngle != 0 && robjectid != WORDNULL) {
                if (!Observer->status->object[_unit->get_robjectid()]->fdestroy) {
                    if (rastpnt(_unit->pnt, Observer->status->object[_unit->get_robjectid()]->pnt) < 1.2 * _unit->WeaponList[0]->radius) {
                        _unit->action = _unit->raction;
                        return;
                        //lock on current target, till it is in 1.2*radius zone for Cirling Units
                    }
                }
            }
            Locate_Enemy();
        }
        
        
        // Attack unit directly
        if (_unit->action == atAttackUnitExactly) {
            robjectid = _unit->get_robjectid();
            bool ff = false;
            LUNIT unit_robject = Observer->status->unit[robjectid];
            if ((robjectid != WORDNULL) && (unit_robject) && (unit_robject->fdestroy == false)) {
                _unit->set_rpnt(unit_robject->pnt);
                _unit->rtr = unit_robject->unt;
            } else {
                _unit->Set_Instantly_Stop();
            }
        }

        
        // Protection logic for a unit
        if (_unit->action == atHold) {
            robjectid = _unit->get_robjectid();
            float minr = maxlongintc;
            US k = WORDNULL;
            int maxind = 0, ind;
            for (int j = 0; j < CMAXUNIT + CMAXLOCATION ; j++) {
                LOBJECT unit_j = Observer->status->object[j];
                if ((unit_j) && (unit_j->InsideTransport == WORDNULL) && (unit_j->fdestroy == false) && (IsEnemyTeams(Observer, unit_j->GetTeamID(), _unit->GetTeamID()))
                    && (_unit->SC_NeedGoCloser(j) == false) && (ind = _unit->IsCanAttack(j)) && ((ind > maxind) || ((ind == maxind) && ((rastpnt(_unit->pnt, unit_j->pnt)) < minr)))) {
                    maxind = ind;
                    minr = rastpnt(_unit->pnt, unit_j->pnt);
                    k = j;
                }
            }
            if (k != WORDNULL) {
                LOBJECT unit_k = Observer->status->object[k];
                _unit->set_rpnt(unit_k->pnt);
                _unit->set_robjectid(k);
                _unit->rtr = unit_k->unt;
            }
        }
        
        if (_unit->action != atDelete) {
            _unit->oldaction = _unit->action;
            _unit->action = _unit->raction;
        } else {
            _unit->newcommand = false;
        }
    }
}

/*
    Checks command by type and launches appropritate handler for a unit
 
 */
void UnitBattleAI::Check_Command(int actionType, bool fQueue, UNITID objectid, PNT pnt1, PNT pnt2) {
    //? ImproveCommandsOfPlayer
    // command supervisor
    // pnttount eats a lot of CPU time
    if ((_unit != nullptr) && (_unit->InsideTransport == WORDNULL) && (!Observer->game_mode->controlRestricted())) {
        switch (actionType) {
            case atStop:
                _unit->Send_Stop(fQueue, true);
                break;
            case atMoveToPnt:
                _unit->Send_MoveToPnt(Observer->planet->pnttount(pnt1), pnt1, fQueue, true);
                break;
            case atTap:
                _unit->Send_Tap(pnt1, objectid, true);
                break;
            case atAttackPnt:
                _unit->Send_AttackPnt(Observer->planet->pnttount(pnt1), pnt1,  fQueue, true);
                break;
            case atAttackUnit:
                _unit->Send_AttackUnit(objectid, fQueue, true);
                break;
            case atPatrolToPnt:
                _unit->Send_PatrolToPnt(Observer->planet->pnttount(pnt1), pnt1, Observer->planet->pnttount(pnt2), pnt2, fQueue, true);
                break;
            case atHold:
                _unit->Send_Hold(fQueue, true);
                break;
            case atCaptureBld:
                _unit->Send_CaptureBld(objectid, fQueue, true);
                break;
            case atDelete:
                _unit->Send_Delete(fQueue, true);
                break;
        }
    }
}



/*
   Initialize a Unit
 */
UNIT::UNIT(UNIT &src) : UpgradeMadeFlags(MAX_FLAGS)
{
	fatalerror("Doesn't work correct. UNIT constructor " );
	ZeroMemory(&STARTCLEAR, &ENDCLEAR-&STARTCLEAR+1);
	CopyMemory(this, &src, sizeof(UNIT));
	if (this->command)
		this->command = new COMMAND(*src.command);
	if (this->unitai)
		this->unitai = new UNITAI(*src.unitai);
	for (int j = 0; j<=cWeaponList-1; j++)
		WeaponList[j] = new WEAPON(*WeaponList[j]);
	lastanimlist = armordesc->AnimIdle;
	oldanim = lastanimlist->GetRandomAnimDesc();
	lastanimtime = status->obs->currenttime;
	lastsubanimtime = oldanim->GetStartTime();
	old_oldunt = oldunt = unt;
	oldpnt = planepnt;
	oldva = planeva;
	oldrpnt = planerpnt;
	pathstatus.step = Observer->subtik;
    // initialize unit dependent artificial intelligence 
    ai.Init(this);
}