/**
 * Copyright (c) 2017, VironIT and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * 
 * This code is part of VironIT software; Part of Relief app.
 *
 * Node example app -  part of main app 
 *
 * @author   VironIT
 * @version  12.1
 * @since    1.0
 * 
 */    

/**
 * Create and init Express application
 */
this.app = express();
this.app.use(bodyParser.json({ limit: '100mb' }));
this.app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
this.app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
this.app.all('*', function (req, res, next) {
	// Check special request with custom authorization or none authorization
    for (iterate in nonAuthorized) {
        if (req.originalUrl.includes(nonAuthorized[iterate])) return next();
    }
    for (iterate in nonAuthorizedGet) {
        if (req.originalUrl.includes(nonAuthorizedGet[iterate])) {
            if (req.method == 'GET') return next();
        }
    }
	// Check headers for authorization jwt
    if (!req.headers.authorization) return res.status(401).json({
        err: 'Token absent'
    });
	// Check jwt through our cryptoLibrary
    cryptoLibrary.checkToken(req.headers.authorization, function (err, data) {
        if (err) return res.status(err).json({
            err: data
        });
        if (data.subject != 'login') return res.status(401).json({err: 'Bad token subject'});
        req.tokenData = data;
        next();
    });
});
// Create routers and append to Express
this.app.use('/api/crossPromotion', require(__base + 'routers/crossPromotionsRouter.js').crossPromotionsRouter);
this.app.use('/api/crossPromotionTable', require(__base + 'routers/crossPromotionTablesRouter.js').crossPromotionTablesRouter);
this.app.use('/api/asset', require(__base + 'routers/assetsRouter.js').assetsRouter);
this.app.all('*', function (req, res) {
	// Reject all others requests
    return res.status(404).json({
        error: 'Bad request'
    });
});
// Start servers
if (port) {
	// Start test server with custom port
    this.serverNode = http.createServer(this.app);
    this.serverNode.listen(port || 9876, function () {
		// Set started flag for Close server
        this.started = true;
        console.log('Start http server.js on ' + port || 9876);
        cb(true);
    }.bind(this));
}
else {
	// Start main server with custom port
    this.httpsServerNode = https.createServer(https_options, this.app);
    this.httpsServerNode.listen(process.env.npm_config_port || 5444, function () {
		// Set started flag for Close server
        this.started = true;
        console.log('Start https server.js on ' + process.env.npm_config_port || 5444);
        cb(true);
    }.bind(this));
}